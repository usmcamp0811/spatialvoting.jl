### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ e3b1cfd5-106e-49f8-a89c-d2ac9cf286a2
begin
	using Latexify
	using LaTeXStrings
	Latexify.set_default(env=:raw)
	∑(a) = Base.sum(a)
	using Revise
	using SpatialVoting
end

# ╔═╡ e4f02737-ffa7-4d4a-8b69-c417795b0eb2
using RDatasets

# ╔═╡ 6d57d145-bbdb-44a2-8713-236f139aa37b
using DataFrames

# ╔═╡ 230054e4-536a-4e92-b84a-df46ddefc2ec
using Plots

# ╔═╡ 8399bc4a-85db-4506-8d87-4f53e3f65135
SpatialVoting.test(13,3)

# ╔═╡ a0bc76b9-e4db-49b5-bd19-dc84c72b1817
RM(5,6)

# ╔═╡ 670334be-bd02-11eb-1a88-e94b679cc318
md"""
# Spatial Voting

This notebook is the basic to Spatial Voting as dirived from the included papers by Dr. Holger Jaenisch. 
"""

# ╔═╡ 33bc294c-2252-4b5f-82ce-cd5f8d4c7736
md"""
## Data

*X* is a single record in some dataset and each value in *X* is a feature.
"""

# ╔═╡ b712c30d-2063-4de8-a371-6088f7ef0b4a
@latexrun X = [1 2 3]
# @latexrun X = [101.0625 86.8 84.3 82.5 92.7]

# ╔═╡ 943942f8-169b-46e6-a56b-e677d050160c
@latexrun X2 = [-1 2 3]

# ╔═╡ 524aedb1-aa50-4eea-a80a-3b275edd403b
md"## Encoding Functions"

# ╔═╡ af473137-66b4-4580-96db-33eaa66cefcc
md"We can initialize the encoding functions with either $1$ or a $0$ depending on what our data looks like. Here we initialize with $1$."

# ╔═╡ 053ffb89-b1d5-40f8-871b-b6aa1c22c605
@latexrun RM₀ = 1.0

# ╔═╡ e8465d39-fb89-47bd-a9ce-d672c1d91c0a
@latexrun RS₀ = 1.0

# ╔═╡ 1b46575f-2e1f-4a2b-b495-9ef0184cf3a9
@latexrun SM_0 = 1.0

# ╔═╡ f09fdf9e-ee1e-441b-b23a-4682b35f1e7b
@latexrun SS_0 = 1.0

# ╔═╡ c9d9c7f1-d17a-48b8-8bdd-f18a3b2ca675
md"The function $RM$ is effectively a running mean."

# ╔═╡ 4d8598ec-cfbf-4c09-9650-56486e5f0215
@latexrun RMi(rm_prev, x_j) = (rm_prev + x_j) / 2

# ╔═╡ cb154df3-2053-40e7-bc66-0f8cb9706cb3
md"The function $RS$ is like a running standard deviation."

# ╔═╡ a62fbd9c-a205-4f92-be39-de68601c57f2
@latexrun RSi(rs_prev, rm_j, x_j) = (rs_prev + sqrt( (x_j - rm_j)^2 / 2) ) / 2

# ╔═╡ b7929b4c-f19b-404c-9a42-22f5f4aa6347
@latexrun SMi(sm_prev, X_j, j) = sm_prev + ( X_j / j )

# ╔═╡ 26380d66-4561-4628-9195-b800c83f06a6
@latexrun SSi(ss_prev, sm_j, x_j, n) = sqrt((ss_prev + ( (x_j - sm_j) / n)^2 ) / n )

# ╔═╡ 0de80af5-4969-40cc-893a-14cec2b84c97
RM(1.0,12)

# ╔═╡ 8c6feeee-aae4-4eae-b2c7-bbcc41c8c8a7
md"One of the nice characteristics of the _Spatial Voting_ method is that the encoding functions can be done together in a single pass over the data."

# ╔═╡ 518a5b88-1697-41d3-9d25-15a27ef4e26a
# function encode(X::Array, sm::Array, ss::Array, rm::Array, rs::Array)
# 	n = length(X)
# 	for i ∈ 1:n
# 		_rm = RM(rm[i], X[i])
# 		push!(rm, _rm)
# 		_rs = RS(rs[i], rm[i+1], X[i])
# 		push!(rs, _rs)
# 		_sm = SM(sm[i], X[i], i)
# 		_ss = SS(ss[i], _sm, X[i], i)
# 		push!(sm, _sm)
# 		push!(ss, _ss)
# 	end	
# 	# remove the initialization value from our data
# 	# popfirst!(sm);
# 	# popfirst!(ss);
# 	# popfirst!(rm);
# 	# popfirst!(rs); 
# 	return sm[end], ss[end], rm[end], rs[end]
# end

# ╔═╡ 7d441097-364a-4ea5-82e2-0d76edba12ca
md"## Grid Functions"

# ╔═╡ 942cd8b6-02e8-4060-8674-59711f08327c
# @latexrun min_max_scaler(X, i; size_grid=5) = floor( ( (X[i]-minimum(X))/ (maximum(X) - minimum(X)) ) * size_grid )

# ╔═╡ 512f4d75-fcf1-477c-b79b-10b1a08615fd
iris = dataset("datasets", "iris")

# ╔═╡ 8183d5ec-6b01-4ce1-ac9e-c3ad418a9946
data = iris[!,1:4] |> Array

# ╔═╡ 1dfa78b4-ee39-45aa-a34e-3c7bc086ba39
begin
	plot(x, y, seriestype = :scatter, xlims=(0,5), ylims=(0,5), xticks=0:1:5, yticks=0:1/4:1, label="X₁")
	# plot!([ix2],[iy2],seriestype = :scatter, label="X₂")
end

# ╔═╡ 08f00e23-6934-4ff5-9920-1517696f8dfc
@latexrun min_max_scaler(X,xmin,xmax,n_grid) = floor((X - xmin) * (n_grid - 1) / (xmax - xmin )) 

# ╔═╡ 18511881-8c92-4f88-9466-cac1941dd1c8
min_max_scaler(X::SVFeature,xmin,xmax,n_grid) = min_max_scaler(X.v,xmin,xmax,n_grid)

# ╔═╡ e301ea4f-e9cc-4e88-b5f2-fd0ef4bc2c8f
min_max_scaler(X::Array; size_grid=5) = [min_max_scale(X,i; size_grid) for i in 1:length(X)]

# ╔═╡ 5e1ad5f2-5b04-4d2f-b668-1eff46f9f61b
struct SVEncoding
	x
	y
	keyval

	function SVEncoding(X::Array, 
			SVFeature_x::String, 
			SVFeature_y::String, 
			xgrid_range::UnitRange, 
			ygrid_range::UnitRange, 
			grid_cells::Int
		)
	encoded_dict = encode(X)
	x = min_max_scaler(
			encoded_dict[SVFeature_x],
			minimum(xgrid_range),
			maximum(xgrid_range),
			grid_cells
		)
	y = min_max_scaler(
			encoded_dict[SVFeature_y],
			minimum(ygrid_range),
			maximum(ygrid_range),
			grid_cells
		)
	key = keyval(x,y,grid_cells)
	new(x,y,key)
	end
end

# ╔═╡ ee8990f1-f5f4-49cf-b64b-cd0c33f4efca
sv = SVEncoding(X,"rm", "rs", 0:5, 0:1, 4)

# ╔═╡ 82732700-7be6-4b8c-9ab7-9f581e176627
@latexrun cell_center(X,xmin,xmax,n_grid) = (xmin + min_max_scaler(X,xmin,xmax,n_grid)) * ( ( xmax - xmin ) / n_grid ) + ( ( xmax - xmin ) / 2n_grid )

# ╔═╡ 3820cae8-63e5-459b-8dbb-b5581b456c48
min_max_scaler(rm,2)

# ╔═╡ 66a5fda8-e794-46cc-a51b-b8300816f60a
cell_center(X::Array; size_grid=5) = [cell_center(X,i; size_grid=size_grid) for i in 1:length(X)]

# ╔═╡ c67a68f3-d198-4b16-8a73-b24b50dc7e1c
cell_center(rm,0,5,4)

# ╔═╡ 7816b27c-f4b7-4095-8cf7-176725a9b126
cell_center(rm,3)

# ╔═╡ e60e0533-0c76-4a8d-a041-e4132b01b16f
keyval(x,y,nsize) = [nsize*y[i] + x[i] for i in 1:length(x)]

# ╔═╡ ac4e51d7-0ba6-4d25-9944-691cff874af5
# calc a grid
sv_grid(xmin::Int,xmax::Int,ymin::Int,ymax::Int) = [xmin:xmax; ymin:ymax]

# ╔═╡ bb42ab52-df7b-41e8-b069-974e3f8d1004


# ╔═╡ Cell order:
# ╠═e3b1cfd5-106e-49f8-a89c-d2ac9cf286a2
# ╠═8399bc4a-85db-4506-8d87-4f53e3f65135
# ╠═a0bc76b9-e4db-49b5-bd19-dc84c72b1817
# ╟─670334be-bd02-11eb-1a88-e94b679cc318
# ╟─33bc294c-2252-4b5f-82ce-cd5f8d4c7736
# ╠═b712c30d-2063-4de8-a371-6088f7ef0b4a
# ╠═943942f8-169b-46e6-a56b-e677d050160c
# ╟─524aedb1-aa50-4eea-a80a-3b275edd403b
# ╟─af473137-66b4-4580-96db-33eaa66cefcc
# ╟─053ffb89-b1d5-40f8-871b-b6aa1c22c605
# ╟─e8465d39-fb89-47bd-a9ce-d672c1d91c0a
# ╟─1b46575f-2e1f-4a2b-b495-9ef0184cf3a9
# ╟─f09fdf9e-ee1e-441b-b23a-4682b35f1e7b
# ╟─c9d9c7f1-d17a-48b8-8bdd-f18a3b2ca675
# ╠═4d8598ec-cfbf-4c09-9650-56486e5f0215
# ╟─cb154df3-2053-40e7-bc66-0f8cb9706cb3
# ╠═a62fbd9c-a205-4f92-be39-de68601c57f2
# ╠═b7929b4c-f19b-404c-9a42-22f5f4aa6347
# ╠═26380d66-4561-4628-9195-b800c83f06a6
# ╠═0de80af5-4969-40cc-893a-14cec2b84c97
# ╟─8c6feeee-aae4-4eae-b2c7-bbcc41c8c8a7
# ╠═518a5b88-1697-41d3-9d25-15a27ef4e26a
# ╟─7d441097-364a-4ea5-82e2-0d76edba12ca
# ╠═942cd8b6-02e8-4060-8674-59711f08327c
# ╠═5e1ad5f2-5b04-4d2f-b668-1eff46f9f61b
# ╠═ee8990f1-f5f4-49cf-b64b-cd0c33f4efca
# ╠═e4f02737-ffa7-4d4a-8b69-c417795b0eb2
# ╠═512f4d75-fcf1-477c-b79b-10b1a08615fd
# ╠═6d57d145-bbdb-44a2-8713-236f139aa37b
# ╠═8183d5ec-6b01-4ce1-ac9e-c3ad418a9946
# ╠═1dfa78b4-ee39-45aa-a34e-3c7bc086ba39
# ╠═82732700-7be6-4b8c-9ab7-9f581e176627
# ╠═08f00e23-6934-4ff5-9920-1517696f8dfc
# ╠═18511881-8c92-4f88-9466-cac1941dd1c8
# ╠═c67a68f3-d198-4b16-8a73-b24b50dc7e1c
# ╠═e301ea4f-e9cc-4e88-b5f2-fd0ef4bc2c8f
# ╠═3820cae8-63e5-459b-8dbb-b5581b456c48
# ╠═66a5fda8-e794-46cc-a51b-b8300816f60a
# ╠═7816b27c-f4b7-4095-8cf7-176725a9b126
# ╠═e60e0533-0c76-4a8d-a041-e4132b01b16f
# ╠═230054e4-536a-4e92-b84a-df46ddefc2ec
# ╠═ac4e51d7-0ba6-4d25-9944-691cff874af5
# ╠═bb42ab52-df7b-41e8-b069-974e3f8d1004
