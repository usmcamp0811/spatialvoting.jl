### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 91b8850e-ac70-4108-9dfd-759039667b31
import Pkg; Pkg.add("PlutoUI")

# ╔═╡ 3376dde6-df8d-11eb-34eb-574262a1d46f
begin
	using Revise
	using SpatialVoting
	using LinearAlgebra, Statistics
	using IntervalSets
	using RDatasets
	using DataFrames
	using Plots
	using PlutoUI
	
	function get_data(;labels::Bool=false)
	    iris = dataset("datasets", "iris")
	    sort!(iris)
	    data = iris[!,1:4] |> Array
	    data = collect(eachrow(data))
	    if labels==true
	        return iris.Species
	    end
	    return convert(Array{Any},data)

	end
	
end

# ╔═╡ f8b17a92-fc55-4180-b6bd-f8f24d82a1cf
data = get_data()

# ╔═╡ 9e10210b-d5a2-4ecc-8c50-19dd2f3d2062
begin
	df = copy(data)
	to_ascii!(df)
	df = convert(Array{Any},df)
	AllCompressiveEncodingStats!(df)
	df = DataFrame(df)
	for col=names(df)
		rename!(df,col => Symbol(uppercase(col)))
	end
	df
end

# ╔═╡ 1b82de37-2daa-42f2-bfb9-c02cbe7a29ab
default_bins = Int(round(√(length(data))))

# ╔═╡ 1300bae9-567b-4ced-9fc2-5bca7fe5e5a8
md"""
X: $(@bind x Select(["RM" => :RM, "RS" => :RS, "SM" => :SM, "SS" => :SS, "TM" => :TM, "TS" => :TS]))  Y: $(@bind y Select(["RM" => :RM, "RS" => :RS, "SM" => :SM, "SS" => :SS, "TM" => :TM, "TS" => :TS])) # of XBins: $(@bind xbins Slider(1:100; default=default_bins, show_value=true)) # of YBins: $(@bind ybins Slider(1:100; default=default_bins, show_value=true))
"""

# ╔═╡ 13609daa-822f-417e-866b-2d98a5513bbb
svgrid,keyvals,anomalies = SVGrid(df,data,Symbol(x),Symbol(y);xbins=xbins,ybins=ybins,ascii=false)

# ╔═╡ 8b5b36c5-afd9-439a-9b82-61b5a47d60be
begin
	ugrids = []
	for k=unique(keyvals)
		key = join(split(k,".")[1:end-1][1:end],".")
		push!(ugrids, key)
	end
	ugrids = Set(ugrids)
end

# ╔═╡ 5072945f-865f-49e3-8e21-c20f56f45c26
md"""
Grid: $(@bind cgrid Select([k => k for k in ugrids]))
"""

# ╔═╡ 4dec82d8-20f4-4422-affa-754f62853bbd
current_grid = cell(cgrid,svgrid)

# ╔═╡ c8a7c5df-c933-4482-b15d-0dfdef554379
current_grid.parent

# ╔═╡ 8d4958d0-badb-42ce-896a-8489ec21d9e8
plot(current_grid)

# ╔═╡ 20755a68-bdab-43a5-b50b-ee9d15599c66
md"Anomalies: $(length(anomalies))"

# ╔═╡ 92484f2a-55e4-4a0d-930b-4f3638d582d2
resolve!(svgrid,df,data,keyvals,anomalies,Symbol(x),Symbol(y))

# ╔═╡ b337c169-56e0-4a69-af20-118e2ea26737
labels = get_data(labels=true)

# ╔═╡ cf23e2c0-cdb4-4b12-8762-62a5d4e23a32
SpatialVoting.plot_labels(current_grid,keyvals,labels)

# ╔═╡ 9a5d6186-f382-4d7b-b895-e77adbcaa52e
SpatialVoting.plot_labels(svgrid,keyvals,labels)

# ╔═╡ baf89fcf-2d82-4b54-9064-263243c06b4c
gdata = SpatialVoting.get_label_grid(svgrid.grid,keyvals,length(svgrid.x)-1,length(svgrid.y)-1,labels) |> Array

# ╔═╡ Cell order:
# ╠═91b8850e-ac70-4108-9dfd-759039667b31
# ╟─3376dde6-df8d-11eb-34eb-574262a1d46f
# ╟─f8b17a92-fc55-4180-b6bd-f8f24d82a1cf
# ╟─9e10210b-d5a2-4ecc-8c50-19dd2f3d2062
# ╟─13609daa-822f-417e-866b-2d98a5513bbb
# ╠═92484f2a-55e4-4a0d-930b-4f3638d582d2
# ╠═8b5b36c5-afd9-439a-9b82-61b5a47d60be
# ╟─5072945f-865f-49e3-8e21-c20f56f45c26
# ╠═4dec82d8-20f4-4422-affa-754f62853bbd
# ╠═c8a7c5df-c933-4482-b15d-0dfdef554379
# ╟─8d4958d0-badb-42ce-896a-8489ec21d9e8
# ╟─1300bae9-567b-4ced-9fc2-5bca7fe5e5a8
# ╟─20755a68-bdab-43a5-b50b-ee9d15599c66
# ╟─cf23e2c0-cdb4-4b12-8762-62a5d4e23a32
# ╠═9a5d6186-f382-4d7b-b895-e77adbcaa52e
# ╟─1b82de37-2daa-42f2-bfb9-c02cbe7a29ab
# ╠═b337c169-56e0-4a69-af20-118e2ea26737
# ╟─baf89fcf-2d82-4b54-9064-263243c06b4c
