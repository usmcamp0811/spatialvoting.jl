```@meta
CurrentModule = SpatialVoting
```

# SpatialVoting

Documentation for [SpatialVoting](https://github.com/mcamp/SpatialVoting.jl).

```@index
```

```@autodocs
Modules = [SpatialVoting]
```
