### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ fc76fbdc-cc5e-11eb-26e9-27bc71c4e0e9
begin
	using Plots
	using RDatasets
	using Plots
	# using UnicodePlots
	using SpatialVoting
	
end

# ╔═╡ 64ca541e-0f18-40bd-8f6f-2e1929acf50c
md"# SVGrid Process Explained for a Dummy(me)"

# ╔═╡ 791d2348-d9bc-427f-a144-9c58f901ab98
begin
	function get_data(;labels::Bool=false)
	    iris = dataset("datasets", "iris")
	    data = iris[!,1:4] |> Array
	    data = collect(eachrow(data))
	    if labels==true
	        return iris.Species
	    end
	    return data
	end
	
	
	
end

# ╔═╡ 3010669b-4186-4163-ac74-538fd6af9598
function get_color(x)
	if x == "setosa"
		return :red
	end
	if x == "versicolor"
		return :green
	end
	if x == "virginica"
		return :blue
	end
end

# ╔═╡ cc7e37fa-1ab7-4051-b490-625704881d1a
plotly()

# ╔═╡ 25d98c4f-71a2-4f4c-b5c1-43a6ded40b6e
md"""
## Step 1: Create a SV Grid
Since we will be converting all data into ASCII Decimal we will be able to bound
our initial grid space between 0 and 255.

The below grid space is divided up into 289 cells that are spaced 15 units appart. As data is streamed in it will get mapped to a cell on this grid.
"""

# ╔═╡ ffac4fb5-fbe3-4970-ad76-131784d08d4f
data = to_ascii.(get_data())

# ╔═╡ 2e84df8f-4364-489c-87ed-7205f6c44e7b
labels = get_data(labels=true)

# ╔═╡ a6d437a4-536d-40da-945b-a5b26e3ee095
parent_xbins,parent_ybins = 5,5

# ╔═╡ 84e4e714-fd03-4884-a82d-3b5b6937332e
parent_xstep,parent_ystep = 51,51

# ╔═╡ f71d0f81-e660-4ea3-9af5-edb6168bbd71
parent_xrange,parent_yrange = 0:parent_xstep:255,0:parent_ystep:255

# ╔═╡ d7c96768-8371-4e1e-8c16-51a3af4faf97
draw_sv_sub_grid(width, height, parent_x, parent_y) = Shape([parent_x,width+parent_x,width+parent_x,parent_x], [parent_y,parent_y,height+parent_y,height+parent_y])

# ╔═╡ 00a0a8dd-2c45-4f05-94c8-f5806cd0ba88
plot(parent_xrange,parent_yrange, zeros(parent_xbins,parent_ybins), seriestype=:scatter,c=cgrad([:white,:blue,:green,:red]), xticks=parent_xrange,yticks=parent_yrange,xlabel="RM",ylabel="SS")

# ╔═╡ 24fb41e7-8c07-4d06-8140-07b4cd08edbb
md"""
## Step 2: Plot a Point

We will read the first record of our data and processes it with our `CompressiveEncodingStats`. Next we choose two of the stats _(RM x SS shown)_ to use and plot them in our SV Gridspace. 
"""

# ╔═╡ f89e26a1-60ac-41c3-9dc9-1781af73bef6

ces = CompressiveEncodingStats.(data)


# ╔═╡ 51d37acc-20ae-483b-8b2a-118971d21c5d
begin
	X₁ = RM.(ces)[1].v; Y₁ = SS.(ces)[1].v;
	xbin = Int(get_bin_loc(X₁,parent_xbins;min=0,max=255))
	ybin = get_bin_loc(Y₁,parent_ybins;min=0,max=255)
	xbin_left_edge = collect(parent_xrange)[xbin]
	ybin_bottom_edge = collect(parent_yrange)[ybin]
	plot!([X₁],[Y₁],seriestype=:scatter,label="Record-1", marker=labels[1])
	plot!(
		draw_sv_sub_grid(
			parent_xstep,
			parent_ystep,
			xbin_left_edge,
			ybin_bottom_edge
			), opacity=.5, label="Cell Record-1 Lands In")
end

# ╔═╡ 8f3095c9-28a7-47e7-9a23-ff5ff0a5a1bf
md"""
## Step 3: Plot more Points
"""

# ╔═╡ cf84fe8c-2ea8-4343-91cd-1b99572f1347
begin
	for _ in 1:10
		i = Int(rand(1:length(data)))
		Xᵢ,Yᵢ = RM.(ces)[i].v,SS.(ces)[i].v
		plot!([Xᵢ],[Yᵢ],seriestype=:scatter,label="Point-$i",marker=get_color(labels[i]))
	end
		# plot!(draw_sv_sub_grid(15,15,45,0), opacity=.5, label="Cell Point-2 Lands In")
	plot!()
end

# ╔═╡ 05b3e0b0-c96d-4296-930f-d6cc88e244ea
begin
	rec = ces[42]
	x,y = RM(rec),SS(rec)
end

# ╔═╡ 32d80399-434f-4dbd-bbc1-b69c01fdf285
SpatialVoting.calc_keyval(get_bin_loc(x.v,0,255,5),get_bin_loc(y.v,0,255,5),5,5)

# ╔═╡ 9a7eb5df-96cb-47ae-9c4a-3232aaad6bfd
SpatialVoting.calc_keyval(2.25,0.48,4+1,4+1)

# ╔═╡ c70dd535-7e67-4bbe-895a-9957a434d0e3


# ╔═╡ Cell order:
# ╟─64ca541e-0f18-40bd-8f6f-2e1929acf50c
# ╠═fc76fbdc-cc5e-11eb-26e9-27bc71c4e0e9
# ╟─791d2348-d9bc-427f-a144-9c58f901ab98
# ╟─3010669b-4186-4163-ac74-538fd6af9598
# ╠═cc7e37fa-1ab7-4051-b490-625704881d1a
# ╟─25d98c4f-71a2-4f4c-b5c1-43a6ded40b6e
# ╟─ffac4fb5-fbe3-4970-ad76-131784d08d4f
# ╠═2e84df8f-4364-489c-87ed-7205f6c44e7b
# ╠═a6d437a4-536d-40da-945b-a5b26e3ee095
# ╠═84e4e714-fd03-4884-a82d-3b5b6937332e
# ╠═f71d0f81-e660-4ea3-9af5-edb6168bbd71
# ╠═d7c96768-8371-4e1e-8c16-51a3af4faf97
# ╠═00a0a8dd-2c45-4f05-94c8-f5806cd0ba88
# ╟─24fb41e7-8c07-4d06-8140-07b4cd08edbb
# ╠═f89e26a1-60ac-41c3-9dc9-1781af73bef6
# ╠═51d37acc-20ae-483b-8b2a-118971d21c5d
# ╟─8f3095c9-28a7-47e7-9a23-ff5ff0a5a1bf
# ╠═cf84fe8c-2ea8-4343-91cd-1b99572f1347
# ╠═05b3e0b0-c96d-4296-930f-d6cc88e244ea
# ╠═32d80399-434f-4dbd-bbc1-b69c01fdf285
# ╠═9a7eb5df-96cb-47ae-9c4a-3232aaad6bfd
# ╠═c70dd535-7e67-4bbe-895a-9957a434d0e3
