# SpatialVoting

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mcamp.gitlab.io/SpatialVoting.jl/dev)
[![Build Status](https://github.com/mcamp/SpatialVoting.jl/badges/master/pipeline.svg)](https://github.com/mcamp/SpatialVoting.jl/pipelines)
[![Coverage](https://github.com/mcamp/SpatialVoting.jl/badges/master/coverage.svg)](https://github.com/mcamp/SpatialVoting.jl/commits/master)
[![Coverage](https://codecov.io/gh/mcamp/SpatialVoting.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/mcamp/SpatialVoting.jl)
