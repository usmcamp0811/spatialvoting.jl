using Colors
"""
A simple container to hold the colors used for the SVGrid plots

colors: an array of color like things that can be fed into the `color=` part of a plot
count_range: an array that is at least `length(colors) - 1` containing preferably integer values
that are the max count of the corresponding color.
*Note: the first color of `colors` should be the background color of your plot*
"""
struct SVColorScheme
    colors
    count_range
    function SVColorScheme(colors,count_range)
        @assert length(count_range) >= length(colors)-1
        return new(colors,count_range)
    end
    # default colors
    SVColorScheme() = SVColorScheme([:black,:blue,:grey,:yellow,:red],[1,3,6,12])
end

rgb(r::Int,g::Int,b::Int) = RGB(r/255,g/255,b/255)

export rgb

function SVColorScheme(min_count::Int,max_count::Int; colors = [rgb(0,0,0),
                                                      rgb(44, 56, 87),
                                                      rgb(32, 66, 153),
                                                      rgb(0 , 0 , 255),
                                                      rgb(74, 166, 73),
                                                      rgb(47, 176, 46),
                                                      rgb(0 , 255, 0),
                                                      rgb(179, 91, 91),
                                                      rgb(240, 84, 84),
                                                      rgb(255, 0 , 0)
                                                      ])
    if abs(max_count - min_count) <= 3
        colors = [rgb(0,0,0), rgb(0 , 0 , 255), rgb(0 , 255, 0), rgb(255, 0 , 0) ] 
    end
    count_range = collect(range(min_count,max_count,length=length(colors)-1))
    return SVColorScheme(colors,vcat([1.0],count_range))
end


function Base.getindex(svcolor::SVColorScheme,i)
    ix = findall(x-> x > i,svcolor.count_range)
    if length(ix) ≠ 0
        return svcolor.colors[first(ix)]
    else
        return svcolor.colors[end]
    end
end

