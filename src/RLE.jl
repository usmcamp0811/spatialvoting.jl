
mutable struct RLE{V,A,X,B,Y}
    v::V
    x_type::A
    x_extent::X
    y_type::B
    y_extent::Y

end

function RLE(sv::SVGrid)
    flag = 0
    if sv.grid[1] == undef
        flag = 0
    else
        flag = 1
    end
    counter = 0
    rle = Array{Any}([flag])

    for ix=2:length(sv.grid)
        if sv.grid[ix] == undef
            flag = 0
        else
            flag = 1
        end
        counter += 1
        current = typeof(sv.grid[ix]) == SVCell || typeof(sv.grid[ix]) == SVGrid
        prev    = typeof(sv.grid[ix-1]) == SVCell || typeof(sv.grid[ix-1]) == SVGrid
        if prev ≠ current
            push!(rle,counter)
            counter = 0
        end
    end
    if sum(rle[2:end]) < length(sv.grid)
        push!(rle,length(sv.grid)-sum(rle[2:end]))
    end
    return RLE(join(rle,","),sv.x_type,sv.x,sv.y_type,sv.y)
end


function grid(rle::RLE)
    rle_str = parse.(Int,split(rle.v,","))
    cell = Bool(rle_str[1])
    grid = []

    current_cell = cell
    for run in rle_str[2:end]
        current_run = [current_cell for r in 1:run]
        grid = vcat(grid,current_run)
        current_cell = !current_cell
    end
    return reshape(Int.(grid),(length(rle.x_extent)-1,length(rle.y_extent)-1))
end
