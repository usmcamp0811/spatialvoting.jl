using StatsBase: countmap
using DataFrames
# import Base: *
using IntervalSets

abstract type SVFeature <: Number end

"""
This is a data structure containing all the SV Features for a single record
"""
mutable struct AllCompressiveEncodingStats{T}
    RM::T
    RS::T
    SM::T
    SS::T
    TM::T
    TS::T
end

Base.getindex(ces::AllCompressiveEncodingStats{T where T<:Number},f::Symbol) = eval(:($ces.$f))
Base.getindex(ces::AllCompressiveEncodingStats{T where T<:Float64},f::Symbol) = eval(:($ces.$f))
Base.getindex(ces::AllCompressiveEncodingStats{T where T<:Int64},f::Symbol) = eval(:($ces.$f))
Base.getindex(ces::AllCompressiveEncodingStats{Vector{Float64}},j::Int,i::Symbol) = eval(:($ces.$i))[j]
Base.getindex(ces::AllCompressiveEncodingStats{Vector{Int64}},j::Int,i::Symbol) = eval(:($ces.$i))[j]

function Base.round(ces::AllCompressiveEncodingStats; kwargs...) 
  rm = round(ces.RM; kwargs...)
  rs = round(ces.RS; kwargs...)
  sm = round(ces.SM; kwargs...)
  ss = round(ces.SS; kwargs...)
  tm = round(ces.TM; kwargs...)
  ts = round(ces.TS; kwargs...)
  return AllCompressiveEncodingStats(rm,rs,sm,ss,tm,ts)
end


"""
This is the single pass method to calculate all the SV Features.
"""
function AllCompressiveEncodingStats(X::Array, sm::Number, ss::Number, rm::Number, rs::Number, tm::Number, ts::Number; digits=nothing)
    n = length(X)
    @inbounds for i ∈ 1:n
        rm = (rm + X[i]) / 2.0
        rs = (rs + sqrt( (X[i] - rm)^2 / 2.0) ) / 2.0
        sm = (sm +  sum(X[1:i])) / n
        ss = √((ss + (sum(X[1:i]) - sm / n)^2) / n)
        tm = (tm + sm) / 2.0
        ts = (ts + √( (X[i] - tm)^2 / 2.0) ) / 2.0
    end	
    if digits ≠ nothing
      rm = round(rm,digits=digits)
      rs = round(rs,digits=digits)
      sm = round(sm,digits=digits)
      ss = round(ss,digits=digits)
      tm = round(tm,digits=digits)
      ts = round(ts,digits=digits)
    end
    return AllCompressiveEncodingStats(rm,rs,sm,ss,tm,ts)

end

AllCompressiveEncodingStats(X::Array;SM=1.0,SS=1.0,RM=1.0,RS=1.0,TM=1.0,TS=1.0,digits=nothing) = AllCompressiveEncodingStats(X,SM,SS,RM,RS,TM,TS;digits=digits)

function AllCompressiveEncodingStats!(ces::AllCompressiveEncodingStats,X::Array;digits=nothing)
    n = length(X)
    @inbounds for i ∈ 1:n
        ces.RM = (ces.RM + X[i]) / 2.0
        ces.RS = (ces.RS + sqrt( (X[i] - ces.RM)^2 / 2.0) ) / 2.0
        ces.SM = (ces.SM +  sum(X[1:i])) / n
        ces.SS = √((ces.SS + (sum(X[1:i]) - ces.SM / n)^2) / n)
        ces.TM = (ces.TM + ces.SM) / 2.0
        ces.TS = (ces.TS + √( (X[i] - ces.TM)^2 / 2.0) ) / 2.0
    end	
    if digits ≠ nothing
        ces.RM = round(ces.RM,digits=digits)
        ces.RS = round(ces.RS,digits=digits)
        ces.SM = round(ces.SM,digits=digits)
        ces.SS = round(ces.SS,digits=digits)
        ces.TM = round(ces.TM,digits=digits)
        ces.TS = round(ces.TS,digits=digits)
    end
    return ces
end

# WIP
function GPU_AllCompressiveEncodingStats!(X,CES)
    # CES = [ SM, SS, RM, RS, TM, TS ]
    n = length(X)
    for i ∈ 1:length(X)
        CES[3] = (CES[3] + X[i]) / 2.0
        CES[4] = (CES[4] + √( X[i] - CES[3])^2 / 2.0) / 2.0
        CES[1] = (CES[1] + sum(X[1:i])) / n
        CES[2] = √((CES[2] +(sum(X[1:i]) - CES[1] / n)^2) / n)
        CES[5] = (CES[5] + CES[1]) / 2.0
        CES[6] = (CES[6] + √( (X[i] - CES[5])^2 / 2.0) ) / 2.0
    end
    return 
end

function AllCompressiveEncodingStats(X::Array{T, 1}; sm=1.0,ss=1.0,rm=1.0,rs=1.0,tm=1.0,ts=1.0,digits=nothing) where T <: Array
    ces = [AllCompressiveEncodingStats(sm,ss,rm,rs,tm,ts) for x in X]
    Threads.@threads for i=1:length(X)
        AllCompressiveEncodingStats!(ces[i],X[i];digits=digits)
    end
    return ces
end

function AllCompressiveEncodingStats!(X::Array; sm=1.0,ss=1.0,rm=1.0,rs=1.0,tm=1.0,ts=1.0,digits=nothing)
    Threads.@threads for i=1:length(X)
        X[i] = AllCompressiveEncodingStats(X[i],sm,ss,rm,rs,tm,ts;digits=digits)
    end
    return X
end

# just for benchmarking
function stats!(Z::Array)
    Z = convert(Array{Any}, Z)
    to_ascii!(Z)
    AllCompressiveEncodingStats!(Z)
    return Z
end

function DataFrames.DataFrame(aces::Array{AllCompressiveEncodingStats})
    data= [
           Dict(
                "RM" => ces.RM,
                "RS" => ces.RS,
                "SM" => ces.SM,
                "SS" => ces.SS,
                "TM" => ces.TM,
                "TS" => ces.TS
               ) for ces in aces
          ]
    return vcat(DataFrame.(data)...)
end

# function Base.show(io::IO, me::CompressiveEncodingStats)
    # rm,rs,sm,ss,tm,ts = me.rm,me.rs,me.sm,me.ss,me.tm,me.ts
    # print(io, "CompressiveEncodingStats(RM: $rm, RS: $rs, SM: $sm, SS: $ss, TM: $tm, TS: $ts)")
# end

export AllCompressiveEncodingStats



"""
By supplying a `CompressiveEncodingStats` data composite type, we can extract the feature we want
"""
# SS(ces::CompressiveEncodingStats) = SS(ces.ss)
# SM(ces::CompressiveEncodingStats) = SM(ces.sm)
# RS(ces::CompressiveEncodingStats) = RS(ces.rs)
# RM(ces::CompressiveEncodingStats) = RM(ces.rm)
# TM(ces::CompressiveEncodingStats) = TM(ces.tm)
# TS(ces::CompressiveEncodingStats) = TS(ces.ts)

