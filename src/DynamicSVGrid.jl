"""
# DynamicSVGrid

`grid::Dict`      => Keys in this dictionary are of the format `"0.1.23"` where all parent parent cells/grids are separated by `.`

`x_type::Symbol`  => The `Symbol` for the `AllCompressiveEncodingStats` to be used in the `X` domain.

`y_type::Symbol`  => The `Symbol` for the `AllCompressiveEncodingStats` to be used in the `Y` domain.

`count::Integer`  => The number of records that have landed in the grid

"""
mutable struct DynamicSVGrid
    grid::Dict
    x_type::Symbol
    y_type::Symbol
    count::Integer
end

Base.keys(dysvgrid::DynamicSVGrid) = keys(dysvgrid.grid)
cell_size(dysvgrid::DynamicSVGrid) = (x=Float64(abs(first(dysvgrid.grid)[2].x[1] - first(dysvgrid.grid)[2].x[end])),y=Float64(abs(first(dysvgrid.grid)[2].y[1] - first(dysvgrid.grid)[2].y[end])))
Base.minimum(dysvgrid::DynamicSVGrid) = minimum( [ dysvgrid[g].count for g in keys(dysvgrid) ] )
Base.maximum(dysvgrid::DynamicSVGrid) = maximum( [ dysvgrid[g].count for g in keys(dysvgrid) ] )

Base.getindex(svgrid::DynamicSVGrid,kwargs...) = getindex(svgrid.grid, kwargs...)

function Base.getindex(dysvgrid::DynamicSVGrid,key::String) 
    key_chain = split(key,".")
    lvl0 = popat!(key_chain,1)
    key_chain = parse.(Int,key_chain)
    x0,y0 = parse.(Int,split(lvl0,","))
    base_grid = dysvgrid[x0,y0]
    for k in key_chain
        base_grid = base_grid.grid[k]
    end
    return base_grid
end


"""
filter a `DynamicSVGrid` object by the cell index in both X and Y space. 
Basically this will return all `SVCell` | `SVGrid` objects in the `DynamicSVGrid` that 
land between `xrng` and `yrng` where the ranges are real values on an unbounded Cartesian Plain
"""
function Base.getindex(dysvgrid::DynamicSVGrid,xrng::UnitRange,yrng::UnitRange)
    gkeys = sort(DataFrame(keys(dysvgrid)),[:1,:2])
    return map(row-> dysvgrid[row[1],row[2]], eachrow(filter(row-> between(row[1],xrng) && between(row[2],yrng),gkeys)))
end

export cell_size

function DynamicSVGrid!(dsvgrid::DynamicSVGrid,svdata::SVData,x,y;xcell_size::Number=10.0,ycell_size::Number=10.0,xbins::Integer=3,ybins::Integer=3,anomalies_only=false,auto_resolve=false,key_col="key")
    if anomalies_only == true
        anomaly_ixs = findall(x-> x==1,svdata[!,"is_anomaly"])
        svdata.v[!,"is_anomaly"] .= false
        svdata.v[!,"key"] = Array{Union{UndefInitializer, String}, 1}([undef for x in 1:size(svdata.v)[1]])
        for ix=anomaly_ixs
            DynamicSVGrid!(dsvgrid,svdata,x,y,ix;xbins=xbins,ybins=ybins,xcell_size=xcell_size,ycell_size=ycell_size,key_col=key_col) 
        end
    else
        for ix=1:length(svdata)
            DynamicSVGrid!(dsvgrid,svdata,x,y,ix;xbins=xbins,ybins=ybins,xcell_size=xcell_size,ycell_size=ycell_size,key_col=key_col) 
        end
    end
end

function DynamicSVGrid!(dsvgrid::DynamicSVGrid,svdata::SVData,x,y,ix;xcell_size::Number=10.0,ycell_size::Number=10.0,
                        xbins::Integer=3,
                        ybins::Integer=3,
                        auto_resolve=false,
                        key_col="key",
                        max_xgrid_cells=nothing,
                        max_ygrid_cells=nothing
    )
    xgrid,ygrid = gridspace(svdata[ix,x],xcell_size),gridspace(svdata[ix, y],ycell_size)
    if auto_resolve == true
        if (xgrid,ygrid) in keys(dsvgrid.grid)
            SVGrid!(dsvgrid.grid[(xgrid,ygrid)],svdata,x,y,ix;parent_key="$xgrid,$ygrid",key_col=key_col)
        else
            # Anomaly found!
            xlower = xgrid * xcell_size
            xupper = (xgrid * xcell_size) + xcell_size
            ylower = ygrid * ycell_size
            yupper = (ygrid * ycell_size) + ycell_size
            xrange = range(xlower,xupper,length=xbins+1)
            yrange = range(ylower,yupper,length=ybins+1)
            svgrid = create_new_svgrid!(svdata,x,y,ix,xrange,yrange;parent_key="$xgrid,$ygrid",key_col=key_col)
            dsvgrid.grid[(xgrid,ygrid)] = svgrid
        end
    else
        if (xgrid,ygrid) in keys(dsvgrid.grid)
            dsvgrid.grid[(xgrid,ygrid)].count += 1
            key = "$xgrid,$ygrid"
            svdata[ix, key_col] = key
        else
            # Anomaly found!
            xlower = xgrid * xcell_size
            xupper = (xgrid * xcell_size) + xcell_size
            ylower = ygrid * ycell_size
            yupper = (ygrid * ycell_size) + ycell_size
            xrange = range(xlower,xupper,length=xbins+1)
            yrange = range(ylower,yupper,length=ybins+1)
            key = "$xgrid,$ygrid"
            row = svdata[ix,svdata.raw_data_cols] |> Array
            svcell = SVCell(row, nothing, key, xrange, yrange, 1)
            dsvgrid.grid[(xgrid,ygrid)] = svcell
            svdata[ix, key_col] = key
            svdata[ix,"is_anomaly"] = true
        end
    end
end

export DynamicSVGrid
