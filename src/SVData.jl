
"""
Current thought is that this will hold a DataFrame that contains all the CompressiveEncodingStats and the keyvals that are associated with each record


Also want to use this a simpler way of passing all the realtive data to plot or SVGrid method
"""
mutable struct SVData{T}
    v::T
    raw_data_cols::Array
    key_cols::Array
    label_cols::Array
    anom_cols::Array
end

Base.getindex(svd::SVData,kwargs...) = getindex(svd.v,kwargs...)
Base.setindex!(svd::SVData,kwargs...) = setindex!(svd.v,kwargs...)
Base.show(io::IO, svd::SVData) = show(io,svd.v)
DataFrames.names(svdata::SVData{DataFrame}) = names(svdata.v)
Base.size(svd::SVData,kwargs) = size(svd.v, kwargs...)
Base.length(svd::SVData) = size(svd,1)
Base.filter(f,svdata::SVData{DataFrame};view::Bool=false) = filter(f,svdata.v;view=view) 

function Base.setindex!(df::SVData, v::AbstractVector, ::typeof(!), col_ind::DataFrames.ColumnIndex)
    DataFrames.insert_single_column!(df.v, v, col_ind)
    return df
end

function SVData(df::DataFrame;raw_data_cols=[],key_cols=[],label_cols=[],anom_cols=["is_anomaly"])
    svfeature_check(df)
    ces_cols = ["RM","RS","SM","SS","TM","TS"]
    # presumably we haven't done any gridding at this point so everything is not anomaly
    for col in anom_cols
        df[!,col] .= false
    end
    if union(ces_cols, raw_data_cols,key_cols,label_cols,anom_cols) ≠ names(df)
        error("Your DataFrame does not have the correct columns in it or you did not properly specify which fields are the data and/or the keys.")
    end
    filter!(x-> x ∉ label_cols, raw_data_cols)
    return SVData(df,raw_data_cols,key_cols,label_cols,anom_cols)
end

anomalies(svdata::SVData;view=true,anom_col="is_anomaly") = filter(x-> x[anom_col] == true, svdata.v,view=view)[!,Not(anom_col)]

function labels(svdata::SVData;view=true,ix=Symbol("!"))
    if view == true
        @view svdata.v[eval(ix),svdata.label_cols]
    else
        svdata.v[eval(ix),svdata.label_cols]
    end
end

function Base.keys(svdata::SVData;unique_set=true,view=false,ix=Symbol("!"),key_col="key")
    if view == true
        return @view svdata.v[eval(ix),key_col]
    else
        if unique_set == false
            return svdata.v[eval(ix),key_col]
        else
            ks = collect(Set(svdata.v[eval(ix),key_col] |> Array))
            ks = [isassigned(ks,ix) == true ? ks[ix] : "notset" for ix in 1:length(ks)]
            return ks
        end
    end
end

function raw_data(svdata::SVData;view=true,ix=Symbol("!"))
    if view == true
        return @view svdata.v[eval(ix),svdata.raw_data_cols]
    else
        return svdata.v[eval(ix),svdata.raw_data_cols]
    end
end

"""
TODO: Add Doc string!
"""
function convert_to_sv(df::DataFrame; label_cols=[],ascii=true,sv_stats=true,key_cols=["key"],anom_cols=["is_anomaly"],SM=1.0,SS=1.0,RM=1.0,RS=1.0,TM=1.0,TS=1.0)
    raw_data_cols = names(df)
    data = copy(df)
    if sv_stats == true
        if ascii == true
            encoded = convert(Array{Array,1},to_ascii.(eachrow(df[!,Not(label_cols)])))
            ces = AllCompressiveEncodingStats(encoded;sm=SM,ss=SS,rm=RM,rs=RS,tm=TM,ts=TS)
        else
            not_encoded = [Array(x) for x in eachrow(df[!,Not(label_cols)])]
            ces = AllCompressiveEncodingStats(not_encoded;sm=SM,ss=SS,rm=RM,rs=RS,tm=TM,ts=TS)
        end
        ces = DataFrame(ces)
        data = hcat(data,ces)
    end
    for key_col in key_cols
        data[!,key_col] = Array{Union{UndefInitializer,String}}([undef for x in 1:size(data)[1]])
    end
    for anom_col in anom_cols
        data[!,anom_col] = [false for x in 1:size(data)[1]]
    end
    filter!(x-> x ∉ label_cols, raw_data_cols)
    svdata = SVData(data,raw_data_cols,key_cols,label_cols,anom_cols)
    return svdata
end

function svfeature_check(df)
    cols = Set(["RM","RS","SM","SS","TM","TS"])
    features = intersect(Set(names(df)),cols)
    if length(features) == 6
        return true
    else
        error("IncorrectFeatures: The DataFrame you are using does not contain all the CompressiveEncodingStats")
    end
end

export SVData,anomalies,labels,convert_to_sv,raw_data



