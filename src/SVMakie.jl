using Makie
# using GLMakie
using GeometryBasics
using Colors


@recipe(SVPlot,svcell,svcolor) do scene
           Attributes(
               strokecolor=:white,
               strokewidth=0.25,
               backgroundcolor=:black,
               bottomspinecolor=:white,
               leftspinecolor=:white,
               rightspinecolor=:white,
               titlecolor=:white,
               topspinecolor=:white,
               xlabelcolor=:white,
               ylabelcolor=:white,
               xtickcolor=:white,
               ytickcolor=:white
           )
       end

@recipe(SVPlot,svgrid) do scene
           Attributes(
               strokecolor=:white,
               strokewidth=0.25,
               backgroundcolor=:black,
               bottomspinecolor=:white,
               leftspinecolor=:white,
               rightspinecolor=:white,
               titlecolor=:white,
               topspinecolor=:white,
               xlabelcolor=:white,
               ylabelcolor=:white,
               xtickcolor=:white,
               ytickcolor=:white
           )
       end

function plot!(sv::SVPlot{<:Tuple{AbstractVector{<:SVCell}, AbstractVector{<:AbstractRGB}}})
    colors = sv[2]
    svcells  = sv[1]
    
    cells = Node(Rect2D{Float64}[])
    cell_colors = Node(RGB{Float64}[])

    function update_plot(svcells::AbstractVector{<:SVCell},colors::AbstractVector{<:AbstractRGB})
        empty!(cells[])
        empty!(cell_colors[])
        for (svcell, color) in zip(svcells,colors)
            ll_corner = lower_left_corner(svcell)
            c = corners(svcell)
            x_size = abs(diff(collect(Set(c[1])))[1])
            y_size = abs(diff(collect(Set(c[2])))[1])
            cell = GeometryBasics.Rect(Vec(Float64(ll_corner.x),Float64(ll_corner.y)),Vec(Float64(x_size),Float64(y_size)))
            push!(cells[],cell)
            push!(cell_colors[],color)
        end
    end
    Observables.onany(update_plot, svcells, colors)
    update_plot(svcells[],colors[])
    poly!(sv,cells,color=cell_colors,strokewidth=sv[:strokewidth],strokecolor=sv[:strokecolor],aspect_ratio=:equal)
    # poly!(sv)
    sv
end

function plot!(sv::SVPlot{<:Tuple{SVGrid,SVColorScheme}})
    svgrid  = sv[1]
    svcolors = sv[2]
    
    cells = Node(Rect2D{Float64}[])
    cell_colors = Node(RGB{Float64}[])

    function update_plot(grid::SVGrid,colors::SVColorScheme)
        empty!(cells[])
        empty!(cell_colors[])

        # for svcell in filter(g-> typeof(g)==SVCell,grid.grid)
        for ix=1:length(grid.grid)
            svcell = grid.grid[ix]
            if !isdefined(svcell,1)
                xy = CartesianIndices((length(grid.x),length(grid.y)))[ix]
                x,y = xy[1],xy[2]
                parent_key = grid.parent
                svcell = SVCell([],nothing,"$parent_key.$ix",grid.x,grid.y,0)
            end
            ll_corner = lower_left_corner(svcell)
            c = corners(svcell)
            x_size = abs(diff(collect(Set(c[1])))[1])
            y_size = abs(diff(collect(Set(c[2])))[1])
            cell = GeometryBasics.Rect(Vec(Float64(ll_corner.x),Float64(ll_corner.y)),Vec(Float64(x_size),Float64(y_size)))
            color = colors[svcell.count]
            if typeof(color) == Symbol
                color = parse(Colorant,color)
            end
            push!(cells[],cell)
            push!(cell_colors[],color)
        end
    end
    Observables.onany(update_plot, svgrid, svcolors)
    update_plot(svgrid[],svcolors[])
    @show sv[:strokecolor]
    poly!(sv,cells,color=cell_colors,strokewidth=sv[:strokewidth],strokecolor=sv[:strokecolor],aspect_ratio=:equal)
    # poly!(sv)
    sv
end
