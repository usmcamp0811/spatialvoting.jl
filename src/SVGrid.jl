using StatsBase
using CategoricalArrays
using Base.Threads
using IntervalSets
using StatsBase
using Debugger
using Markdown
using DataStructures
using DataFrames
using SparseArrays

"""
# SVCell

This is the smallest SV Unit. It contains an example (1st record seen) of the data in it within the `exemplar` 
property.

`exemplar::array`                                   => example data for this cell

`group::union{number,nothing}`                      => the group of which the cell belongs

`key::string`                                       => the key for the cell. this takes the form of `"0.3.44.5"` which contains all parent cells separated by `.`

`parent_xrange::union{steprange,steprangelen}`      => the parent grid's xrange

`parent_yrange::union{steprange,steprangelen}`      => the parent grid's yrange

`count::integer`                                    => the number of records of data that land within the cell

"""
mutable struct SVCell
    exemplar::Array
    group::Union{Number,Nothing,String}
    key::String
    parent_xrange::Union{StepRange,StepRangeLen,LinRange}
    parent_yrange::Union{StepRange,StepRangeLen,LinRange}
    count::Integer
end

function Base.show(io::IO, svcell::SVCell)
    exemplar =  svcell.exemplar
    if occursin(".",svcell.key) # not a top level cell
        xreal, yreal = realspace(svcell)
    else
        xreal, yreal = split(svcell.key,",")
        try
            xreal, yreal = parse(Int,xreal), parse(Int, yreal)
        catch
            xreal, yreal = parse(BigInt,xreal), parse(BigInt, yreal)
        end

    end
    
    println(io, md"# SVCell")
    println(io, "              Key: ", svcell.key)
    println(io, "         Exemplar: ", exemplar)
    println(io, "     # of Records: ", svcell.count)
    println(io, "                X: ", xreal)
    println(io, "                Y: ", yreal)
    println(io, "         Parent X: ", svcell.parent_xrange)
    println(io, "         Parent Y: ", svcell.parent_yrange)
    println(io, "\n")
end

mutable struct SVGroup
    xbin::Interval{:closed,:open}
    ybin::Interval{:closed,:open}
    parent_xrange::Union{StepRange,StepRangeLen,LinRange,Nothing}
    parent_yrange::Union{StepRange,StepRangeLen,LinRange,Nothing}
    nbins_x::Integer
    nbins_y::Integer
end


"""
# SVGrid

`exemplar::Array`

`grid::Array{Union{SVGrid,SVCell,UndefInitializer}}`   => A 1D Array containing either `SVGrid` or `SVCell` objects. If the cell in the grid that the array represents is unpopulated then it is `undef`.

`x::Union{StepRange,StepRangeLen}`                     => The range of real numbers that `x` values of `CompressiveEncodingStats` values will fall between

`x_type::DataType`                                     => The Compressive Encoding Stat used for the X axis

`y::Union{StepRange,StepRangeLen}`                     => The range of real numbers that `y` values of `CompressiveEncodingStats` values will fall between

`y_type::DataType`                                     => The Compressive Encoding Stat used for the Y axis

`count::Integer`                                       => The number of data points that have been observed to land in this grid

`level::Integer`                                       => A number representing how many parent grids exist for this particular `SVGrid`. It is used to scale data so we avoid rounding errors

`parent::String`                                       => The key / identifier of the parent grid that the current grid is a child of.

The `SVGrid` is the composite type that contains a set of cells in some grid space described by `x` and `y` ranges
"""
mutable struct SVGrid
    exemplar::Array
    grid::Array{Union{String,SVGrid,SVCell,UndefInitializer}}
    x::Union{StepRange,StepRangeLen,LinRange}
    x_type::Symbol
    y::Union{StepRange,StepRangeLen,LinRange}
    y_type::Symbol
    count::Integer
    level::Integer
    parent::String
end


Base.minimum(svgrid::SVGrid) = minimum([ g ≠ undef ? g.count : 0 for g in svgrid.grid ])
Base.maximum(svgrid::SVGrid) = maximum([ g ≠ undef ? g.count : 0 for g in svgrid.grid ])

function Base.show(io::IO, svgrid::SVGrid)
    exemplar = svgrid.exemplar
    println(io, md"# SVGrid")
    println(io, "    Exemplar: ", exemplar)
    xtype = svgrid.x_type
    ytype = svgrid.y_type
    println(io, "           X: $xtype ", svgrid.x)
    println(io, "           Y: $ytype ", svgrid.y)
    println(io, "# of Recrods: ", svgrid.count)
    # println(io, grid(svgrid))
    # println(io, "\n")
end


Base.in(key::Number, g::SVGroup) = key ∈ LinearIndices((length(g.parent_xrange) - 1, length(g.parent_yrange) - 1))[g.xbin.left:g.xbin.right,g.ybin.left:g.ybin.right]
Base.getindex(svgrid::SVGrid,kwargs...) = getindex(svgrid.grid, kwargs...)
between(x::Number,r::R) where {R <: AbstractRange}= first(r) <= x < last(r)

function Base.getindex(dysvgrid::DynamicSVGrid,grp::SVGroup)
    return dysvgrid[grp.xbin.left:grp.xbin.right,grp.ybin.left:grp.ybin.right]
end

export between

function Base.:(==)(a::SVGroup, b::SVGroup)
    bins_equal = (a.xbin == b.xbin) && (a.ybin == b.ybin)
    parents_equal = (a.parent_xrange == b.parent_yrange) && (a.parent_yrange == b.parent_yrange)
    are_equal = (bins_equal == true) && (parents_equal == true)
    return are_equal
end


Base.size(g::SVGrid) = (length(g.x) - 1, length(g.y) - 1)
DataFrames.DataFrame(g::SVGrid) = DataFrame(
                                            exemplar=[g.exemplar],
                                            grid=[g.grid],
                                            x_type=[g.x_type],
                                            xrange=[g.x],
                                            xbins=[length(g.x) - 1],
                                            y_type=[g.y_type],
                                            yrange=[g.y],
                                            ybins=[length(g.y) - 1],
                                            level=[g.level],
                                            count=[g.count],
                                            parent=[g.parent]
                                            )

"""
SVGrid: 

Given an X and Y value, create an SVGrid
"""
function SVGrid(
        x::Number,
        y::Number,
        row::Array,
        xtype::Symbol,
        xrange::Union{StepRange,StepRangeLen,LinRange},
        ytype::Symbol,
        yrange::Union{StepRange,StepRangeLen,LinRange};
        level::Integer=0,
        parent_key::String="0,0",
        key_col="key",
    )

    xbins = length(xrange) - 1
    ybins = length(yrange) - 1

    # if xbins == 3 && ybins == 3
        # should always be the middle cell if we are here.. that cell is 5
        # key = "$parent_key.5"
        # keyn = 5
    # else
    xbin = gridspace(x, xrange)
    ybin = gridspace(y, yrange)
    keyn = keyval(xbin, ybin, (xbins, ybins))
    key = "$parent_key.$keyn"
    # end
    child = SVCell(row, nothing, key, xrange, yrange, 1)
    svgrid = SVGrid(row, Array{Union{SVGrid,SVCell,UndefInitializer}}([undef for _ in 1:xbins * ybins]), xrange, xtype, yrange, ytype, 1, level, parent_key)
    svgrid.grid[keyn] = child
    return svgrid, key, row
end

"""
A function that mutates `SVData` and returns a new `SVGrid` for the point provided at `ix`
"""
function create_new_svgrid!(
        svdata::SVData,
        xtype::Symbol,
        ytype::Symbol,
        ix::Integer,
        xrange::RX,
        yrange::RY;
        level::Integer=0,
        parent_key::String="0,0",
        key_col="key",
        anom_col="is_anomaly"
    ) where {RX <: AbstractRange,RY <: AbstractRange}

    xbins = length(xrange) - 1
    ybins = length(yrange) - 1
    x = svdata[ix,xtype]
    y = svdata[ix,ytype]
    row = raw_data(svdata; ix=ix) |> Array

    xbin = gridspace(x, xrange)
    ybin = gridspace(y, yrange)
    keyn = keyval(xbin, ybin, (xbins, ybins))
    key = "$parent_key.$keyn"

    child = SVCell(row, nothing, key, xrange, yrange, 1)
    svgrid = SVGrid(row, Array{Union{SVGrid,SVCell,UndefInitializer}}([undef for _ in 1:xbins * ybins]), xrange, xtype, yrange, ytype, 1, level, parent_key)
    svgrid.grid[keyn] = child
    svdata[ix,key_col] = key
    svdata[ix,anom_col] = true
    return svgrid
end

"""
Make a new SVGrid

`svdata::SVData`      => This is the data that will be mapped into the new `SVGrid`

`x::Symbol`           => This is the compressive stat or coloumn in the `SVData` that will map to the `X` dimension of the grid

`y::Symbol`           => This is the compressive stat or coloumn in the `SVData` that will map to the `Y` dimension of the grid

`xmin=nothing`        => This should be the min value in the `x` domain.. aka the `first(extent(svcell).x)`. If `nothing` is provided then it will be infered from `SVData`

`xmax=nothing`        => This should be the max value in the `x` domain.. aka the `last(extent(svcell).x)`.  If `nothing` is provided then it will be infered from `SVData`

`ymin=nothing`        => This should be the min value in the `x` domain.. aka the `first(extent(svcell).y)`. If `nothing` is provided then it will be infered from `SVData`

`ymax=nothing`        => This should be the max value in the `x` domain.. aka the `last(extent(svcell).y)`.  If `nothing` is provided then it will be infered from `SVData`

`level::Integer=0`    => This is the level / depth of the `SVGrid`.. might be a hold over as at one point I was doing things to try and avoid rounding errors / bugs

`parent_key::String`  => This is the parent grid ID/Key... if this is the first `SVGrid` then it should be set to `0` else set it to the `key` value of the `SVCell` that we are resolving

`ascii::Bool`         => If the data was encoded in ascii decimal.. might also not be used for anything now

`xbins::Int`          => The number of bins to use in the `X` demension

`ybins::Int`          => The number of bins to use in the `Y` demension


"""
function SVGrid(svdata::SVData,x::Symbol,y::Symbol;
        xmin=nothing,
        xmax=nothing,
        ymin=nothing,
        ymax=nothing,
        level::Integer=0,
        parent_key::String="0",
        ascii::Bool=false,
        xbins::Integer=Integer(round(√(size(svdata, 1)))),
        ybins::Integer=Integer(round(√(size(svdata, 1)))),
        key_col="key",
        anom_col="is_anomaly"
    )
    if xbins < 3
        xbins = 3
    end
    if ybins < 3
        ybins = 3
    end
    if xmin != nothing && ymin != nothing && xmax != nothing && ymax != nothing
        xrange = range(xmin, xmax, length=xbins + 1)
        yrange = range(ymin, ymax, length=ybins + 1)
    else
        xrange = range(minimum(svdata[!,x]) - 0.001, maximum(svdata[!,x]) + 0.001, length=xbins + 1)
        yrange = range(minimum(svdata[!,y]) - 0.001, maximum(svdata[!,y]) + 0.001, length=ybins + 1)
    end
    svgrid = SVGrid([], Array{Union{SVGrid,SVCell,UndefInitializer}}([undef for _ in 1:xbins * ybins]), xrange, x, yrange, y, 0, level, parent_key)
    if parent_key ≠ "0"
        dix = [x[1] for x in findall(x -> x == parent_key, keys(svdata;unique_set=false,view=true) |> Array)]
    else
        dix = 1:size(svdata, 1)
    end
    @inbounds for i = dix
        SVGrid!(svgrid, svdata, x, y, i;ascii=ascii,parent_key=parent_key,key_col=key_col,anom_col=anom_col)
    end

    return svgrid
end

"""
This method will reolve a given SVCell **unless** the resulting `SVGrid` has fewer populated cells than the previous level.

`cell::SVCell`        => A `SVCell` from a previous level grid that we are attempting to resolve.

`svdata::SVData`      => This is the data that will be mapped into the new `SVGrid`

`x::Symbol`           => This is the compressive stat or coloumn in the `SVData` that will map to the `X` dimension of the grid

`y::Symbol`           => This is the compressive stat or coloumn in the `SVData` that will map to the `Y` dimension of the grid

`level::Integer=0`    => This is the level / depth of the `SVGrid`.. might be a hold over as at one point I was doing things to try and avoid rounding errors / bugs

`parent_key::String`  => This is the parent grid ID/Key... if this is the first `SVGrid` then it should be set to `0` else set it to the `key` value of the `SVCell` that we are resolving

`ascii::Bool`         => If the data was encoded in ascii decimal.. might also not be used for anything now

`xbins::Int`          => The number of bins to use in the `X` demension

`ybins::Int`          => The number of bins to use in the `Y` demension

`n_populated`         => The number of populated cells in the parent grid; if `nothing` then we assume we are at level 0
"""
function SVGrid(cell::SVCell, svdata::SVData, x::Symbol, y::Symbol;
        level::Integer=1,
        parent_key::String=cell.key,
        ascii::Bool=true,
        xbins::Integer=Int(round(√(cell.count))),
        ybins::Integer=Int(round(√(cell.count))),
        n_populated=nothing,
        key_col="key",
        anom_col="is_anomaly"
    )

    cell_extent = extent(cell)
    # keep the indices of the records for the cell in case we are at a stopping condition
    cell_indicies = [x[1] for x in findall(x -> x == parent_key, keys(svdata;view=true, unique_set=false) |> Array)]
    svgrid = SVGrid(svdata,
                     x,
                     y;
                     xmin=minimum(cell_extent.x),
                     xmax=maximum(cell_extent.x),
                     ymin=minimum(cell_extent.y),
                     ymax=maximum(cell_extent.y),
                     level=level,
                     parent_key=parent_key,
                     ascii=ascii,
                     xbins=xbins,
                     ybins=ybins,
                     key_col=key_col,
                     anom_col=anom_col
                    )
    new_populated_cells = sum([ x ≠ undef ? 1 : 0 for x in svgrid.grid])
    # @show new_populated_cells, n_populated
    if new_populated_cells > n_populated
        return svgrid
    else
        # reset SVData with original key value
        svdata.v[cell_indicies,key_col] .= cell.key
        return cell
    end
end

function SVGrid!(svgrid::SVGrid, svdata::SVData, x::Symbol, y::Symbol, ix::Integer;parent_key::String="0",ascii::Bool=false,key_col="key",anom_col="is_anomaly")
    if first(svgrid.x) == 0
        xrange_start = first(svgrid.x) * 10^svgrid.level
    else
        xrange_start = first(svgrid.x) * 10^svgrid.level
    end
    if first(svgrid.y) == 0
        yrange_start = first(svgrid.y) * 10^svgrid.level
    else
        yrange_start = first(svgrid.y) * 10^svgrid.level
    end
    X = raw_data(svdata;ix=ix) |> Array
    xbin = gridspace(svdata[ix,x], minimum(svgrid.x), maximum(svgrid.x), length(svgrid.x) - 1)
    ybin = gridspace(svdata[ix,y], minimum(svgrid.y), maximum(svgrid.y), length(svgrid.y) - 1)
    keyn = keyval(xbin, ybin, svgrid)
    key = "$parent_key.$keyn"
    # TODO: Revist and make new column per level
    svdata[ix,key_col] = key
    if length(svgrid.exemplar) == 0
        if ascii == true
            # save off the very first thing we see as an example observation
            svgrid.exemplar = X
        else
            svgrid.exemplar = X
        end
    end
    if svgrid.grid[keyn] != undef
        svgrid.grid[keyn].count += 1
        if typeof(svgrid.grid[keyn]) == SVGrid
            SVGrid!(svgrid.grid[keyn], svdata, x, y, ix;anom_col=anom_col)
            return
        end
    else
        # This is an anomaly / never seen before thing
        child = SVCell(X, nothing, key, svgrid.x, svgrid.y, 1)
        svgrid.grid[keyn] = child
        if length(X) > 0
            svdata[ix,anom_col] = true
        end
    end
    svgrid.count += 1
return 
end

function groups(dysvgrid::DynamicSVGrid; nbins::Integer=0)
    gkeys = sort(DataFrame(keys(dysvgrid)),[:1,:2])
    minmax = describe(gkeys)
    xrange = BigInt(minmax[1,"min"]-1):BigInt(1):BigInt(minmax[1,"max"]+1)
    yrange = BigInt(minmax[2,"min"]-1):BigInt(1):BigInt(minmax[2,"max"]+1)
    # calc gridspace because we could have negative space values here
    xlist = map(x-> gridspace(x,xrange),gkeys[!,1])
    ylist = map(y-> gridspace(y,yrange),gkeys[!,2])
    xcellstart, xcellend = dy_calc_start_stop(xlist)
    ycellstart, ycellend = dy_calc_start_stop(ylist)
    xcellstart = [xrange[x] for x in xcellstart]
    xcellend = [xrange[x] for x in xcellend]
    ycellstart = [yrange[y] for y in ycellstart]
    ycellend = [yrange[y] for y in ycellend]

    x = []
    # pairwise combine all x points to give start and end values in the x domain
    for xs in xcellstart, xe in xcellend
        if xs ≤ xe
            push!(x, (xstart = xs, xstop = xe))
        end
    end
    y = []
    # pairwise combine all y points to give start and stop values in the y domain
    for ys in ycellstart, ye in ycellend
        if ys ≤ ye
            push!(y, (ystart = ys, ystop = ye))
        end
    end

    xy = []
    xy = []
    # pairwise combine all x and y ranges to give you the corners of each group
    @show length(x), length(y)
    i = 0
    for xx in x
Threads.@threads        for yy in y
            x_cell_range = BigInt(xx.xstart):BigInt(xx.xstop)
            y_cell_range = BigInt(yy.ystart):BigInt(yy.ystop)
            if nbins > 3
                xy_n_cells = nbins
            else
                # number of cells in the group
                cells_in_range = dysvgrid[x_cell_range,y_cell_range]
                if length(cells_in_range) > 0
                    xy_n_cells = Integer(ceil(√sum([c.count for c in cells_in_range])))
                else
                    xy_n_cells = 0
                end
            end
            if xy_n_cells < 3 
                xy_n_cells = 3
            end
            if length(cells_in_range) > 0
                i += 1
                xbin = Interval{:closed,:open}(first(x_cell_range),last(x_cell_range))
                ybin = Interval{:closed,:open}(first(y_cell_range),last(y_cell_range))
                grp = SVGroup(xbin, ybin, nothing, nothing, xy_n_cells, xy_n_cells)
                if typeof(grp) ≠ SVGroup
                    @show xx,yy,xbin,ybin
                end
                push!(xy,grp)
                @show i, xy[i]
            end
        end
    end
    return xy
end

"""
Returns a list of SVGroups given some SVGrid
`nbins` is the number of cells to use in the group. If `< 4` 
we will use the formula `√number_points`
"""
function groups(svgrid::SVGrid; nbins::Integer=0)
    grd = grid(svgrid) |> Array
    xlist, ylist = which_rows_cols_populated(grd)
    xcellstart, xcellend = calc_start_stop(xlist)
    ycellstart, ycellend = calc_start_stop(ylist)
    x = []

    # pairwise combine all x points to give start and end values in the x domain
    for xs in xcellstart, xe in xcellend
        if xs ≤ xe && 0 ∉ xlist[xs:xe - 1]
            push!(x, (xstart = xs, xstop = xe))
        end
    end
    y = []
    # pairwise combine all y points to give start and stop values in the y domain
    for ys in ycellstart, ye in ycellend
        if ys ≤ ye && 0 ∉ ylist[ys:ye - 1]
            push!(y, (ystart = ys, ystop = ye))
        end
    end

    xy = []
    # pairwise combine all x and y ranges to give you the corners of each group
    for xx in x, yy in y
        x_cell_range = Interval{:closed,:open}(xx.xstart, xx.xstop)
        y_cell_range = Interval{:closed,:open}(yy.ystart, yy.ystop)
        if nbins > 3
            xy_n_cells = nbins
        else
            # number of cells in the group
            xy_n_cells = Int(ceil(√sum(grd[x_cell_range.left:x_cell_range.right,y_cell_range.left:y_cell_range.right])))
        end
        if xy_n_cells < 3 
            xy_n_cells = 3
        end
        if sum(grd[x_cell_range.left:x_cell_range.right,y_cell_range.left:y_cell_range.right]) > 0
            push!(xy, SVGroup(x_cell_range, y_cell_range, svgrid.x, svgrid.y, xy_n_cells, xy_n_cells))
        end
    end
    return xy
end

function resolve!(svgrid::SVGrid,svdata::SVData;
                  prev_key::String="",
                  level::Integer=1,
                  prev_cells::Integer=1,
                  stop_level::Integer=10,
                  ascii::Bool=true,
                  xbins=nothing,
                  ybins=nothing,
                  key_col="key",
                  anom_col="is_anomaly"
    )
    current_lvl = svgrid.level
    n_populated = sum([ x ≠ undef ? 1 : 0 for x in svgrid.grid])
    for ix = 1:length(svgrid.grid)
        cell = svgrid.grid[ix]
        if cell ≠ undef
            if cell.count > 1 
                if typeof(cell) == SVCell
                    svgrid.grid[ix] = SVGrid(cell, svdata, svgrid.x_type, svgrid.y_type;level=svgrid.level + 1,n_populated=n_populated, key_col=key_col,anom_col=anom_col)
                elseif typeof(cell) == SVGrid
                    grid_key = cell.parent
                    if prev_key == "$grid_key.$ix"
                        return
                    end
                    resolve!(svgrid, svdata;prev_key="$grid_key.$ix",level=svgrid.level + 1,key_col=key_col,anom_col=anom_col)
                end
            end
        end
    end
end

function resolve!(dysvgrid::DynamicSVGrid, svdata::SVData; anomalies_only=false,key_col="key",anom_col="is_anomaly")
    ks = collect(keys(dysvgrid))
    for k = 1:length(ks)
        key = ks[k]
        sv = dysvgrid[key]
        if typeof(sv) <: SVGrid
            resolve!(sv, svdata;key_col=key_col,anom_col=anom_col)
        else
            dysvgrid.grid[key] = SVGrid(sv, svdata, dysvgrid.x_type, dysvgrid.y_type;level=1,n_populated=1, key_col=key_col,anom_col=anom_col)
        end
    end
end

"""
This is presently an attempt at making `resolve!` be parallelize-able... 
This seemingly works as is now.. but need to do some stuff to integrate results across a bunch of runs of this function
Thinking that the `data` and `keyvals` passed in should only be the ones belonging to the `svgrid` we are resolving
This would allow us to more easily integrate the results from multiple runs on different grids
Probably can do something with DataFrames to store an original index so we don't get our of order with the source data
"""
function resolve(svgrid::SVGrid, ces::DataFrame, data::Array, keyvals::Array, x::Symbol, y::Symbol;prev_key::String="",level::Integer=1,prev_cells::Integer=1,stop_level::Integer=10,ascii::Bool=true,key_col="key",anom_col="is_anomaly")
    anomalies = []
    kvs = copy(keyvals)
    resolve!(svgrid, ces, data, kvs, anomalies, x, y;level=level + 1,prev_cells=length(unique(kvs)),prev_key=prev_key,stop_level=stop_level,ascii=ascii,key_col=key_col,anom_cols=anom_cols)
    return svgrid, kvs, anomalies
end



function groups!(svgrid::SVGrid)
    old_groups = svgrid.groups
    if length(old_groups) == 0
        i = 1
        for grp in groups(svgrid)
            svgrid.groups[i] = grp
            i += 1
        end
    else
        println("I don't know what to do now")
        # do i integrate the over lapping grids or what?
    end
end


function infer_keyval(dysvgrid::DynamicSVGrid, keyvals::Array, x::Real, y::Real)
    for key in keyvals
        if (x, y) in dysvgrid[key]
            return key
        end
    end
    return "anomaly"
end

export calc_start_stop, SVGroup, groups, groups!, infer_keyval
export filter_data, cell
export SVCell, gridspace_slow
export resolve, resolve!
export gridspace
export SVGrid!, grid

