using Revise
using RDatasets
# using Plots
# using UnicodePlots
using SpatialVoting
using MLDatasets


function get_data(;labels::Bool=false)
    iris = dataset("datasets", "iris")
    data = iris[!,1:4] |> Array
    data = collect(eachrow(data))
    if labels==true
        return iris.Species
    end
    return data
end

data = get_data()
# mnist_x, mnist_y = MNIST.traindata()
# mnist_x = reshape(mnist_x, (28*28,size(mnist_x)[3]))
# data = collect(eachcol(mnist_x))
# data = to_ascii.(data)
data = to_ascii.(data)
# ces = CompressiveEncodingStats.(data)
# x = [v.v for v in RM.(ces)]
# y = [v.v for v in SS.(ces)]

sqr_points = 2Int(round(sqrt(length(ces))))
# sqr_points = round(Int(round(sqrt(length(ces)))) / 2)
xsize, ysize = sqr_points, sqr_points

svgrid,keyvals = SVGrid(data,RM,SS,0:17:255,0:17:255)


# keys = keyval.(points)
hist = SpatialVoting.get_grid(svgrid) |> Array
p1 = heatmap(1:xsize,
           1:ysize, hist,
           c=cgrad([:black, :blue, :green,:yellow, :red]),
           xlabel="RM", ylabel="SS",
           title="SpatialVoting Grid: Counts") 



hist = SVDict(data,RM,SS,xsize,ysize)
grid = SpatialVoting.get_grid(hist,xsize,ysize) |> Array
p1 = heatmap(1:xsize,
           1:ysize, grid,
           c=cgrad([:black, :blue, :green,:yellow, :red]),
           xlabel="RM", ylabel="SS",
           title="SpatialVoting Grid: Counts") 


hist = SpatialVoting.get_purity_grid(svgrid,get_data(labels=true)) |> Array
# hist = SpatialVoting.get_purity_grid(svgrid,mnist_y) |> Array
p2 = heatmap(1:xsize,
           1:ysize, hist,
           c=cgrad([:black, :blue, :green,:yellow, :red]),
           xlabel="RM", ylabel="SS",
           title="SpatialVoting Grid: Number of Different Classes") 


hist = SpatialVoting.get_label_grid(svgrid,iris.Species) |> Array

p3 = heatmap(1:xsize,
           1:ysize, hist,
           c=cgrad([:black, :blue, :green,:yellow, :red]),
           xlabel="RM", ylabel="SS",
           title="SpatialVoting Grid: Classes") 
# xx = [v.x.v for v in points]
# yy = [v.y.v for v in points]
# histogram2d(xx,yy,bins=grid.n_bins)
# histogram2d(xx,yy,grid=1)
savefig(p1,"SVGrid-IrisDataset-Counts.png")
savefig(p2,"SVGrid-IrisDataset-Purity.png")
savefig(p3,"SVGrid-IrisDataset-Classes.png")
# plot(xx,yy,seriestype = :scatter)

data = [[1,2,3], [99,4,3], [44,34,56],[5455,3,4], [1,2,4], [1,1,3], [1,2,3], [44434,43,44]]
data = get_data()
ces = CompressiveEncodingStats.(to_ascii.(data))
x,y = RM.(ces),SS.(ces)
nbins = 24
hist = SpatialVoting.get_grid(SVGrid(x,y,nbins,nbins)) |> Array
p3 = heatmap(1:nbins,
           1:nbins, hist,
           c=cgrad([:black, :blue, :green,:yellow, :red]),
           xlabel="RM", ylabel="SS",
           title="SpatialVoting Grid: Classes") 

data = get_data()
nbins = 17
hist = SVDict(data,RM,SS,nbins,nbins;xmin=47.38,xmax=51.97,ymin=0.803,ymax=0.942)
# hist = SVDict(data,RM,SS,nbins,nbins;xmin=0,xmax=255,ymin=0,ymax=255)
grid = SpatialVoting.get_grid(hist,nbins,nbins) |> Array
p3 = heatmap(1:nbins,
           1:nbins, grid,
           c=cgrad([:black, :blue, :green,:yellow, :red]),
           xlabel="RM", ylabel="SS",
           title="SpatialVoting Grid: Classes") 

